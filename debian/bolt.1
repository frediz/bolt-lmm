.\"
.TH BOLT "1" "May 2018" "2.3.2"
.SH NAME
bolt \- Efficient large cohorts genome-wide Bayesian mixed-model association testing
.SH SYNOPSIS
.B bolt
[\fIoptions\fR]
.SH DESCRIPTION
The BOLT-LMM software package currently consists of two main algorithms, the
BOLT-LMM algorithm for mixed model association testing, and the BOLT-REML
algorithm for variance components analysis (i.e., partitioning of
SNP-heritability and estimation of genetic correlations).

The BOLT-LMM algorithm computes statistics for testing association between
phenotype and genotypes using a linear mixed model. By default, BOLT-LMM
assumes a Bayesian mixture-of-normals prior for the random effect attributed
to SNPs other than the one being tested. This model generalizes the standard
infinitesimal mixed model used by previous mixed model association methods,
providing an opportunity for increased power to detect associations while
controlling false positives. Additionally, BOLT-LMM applies algorithmic
advances to compute mixed model association statistics much faster than
eigendecomposition-based methods, both when using the Bayesian mixture model
and when specialized to standard mixed model association.

The BOLT-REML algorithm estimates heritability explained by genotyped SNPs and
genetic correlations among multiple traits measured on the same set of
individuals. BOLT-REML applies variance components analysis to perform these
tasks, supporting both multi-component modeling to partition SNP-heritability
and multi-trait modeling to estimate correlations. BOLT-REML applies a Monte
Carlo algorithm that is much faster than eigendecomposition-based methods for
variance components analysis at large sample sizes.
.SH OPTIONS
\fB\-h\fR [ \fB\-\-help\fR ]
print help message with typical options
.TP
\fB\-\-helpFull\fR
print help message with full option list
.TP
\fB\-\-bfile\fR arg
prefix of PLINK .fam, .bim, .bed files
.TP
\fB\-\-bfilegz\fR arg
prefix of PLINK .fam.gz, .bim.gz, .bed.gz
files
.TP
\fB\-\-fam\fR arg
PLINK .fam file (note: file names ending in
\&.gz are auto\-[de]compressed)
.TP
\fB\-\-bim\fR arg
PLINK .bim file(s); for >1, use multiple
\fB\-\-bim\fR and/or {i:j}, e.g., data.chr{1:22}.bim
.TP
\fB\-\-bed\fR arg
PLINK .bed file(s); for >1, use multiple
\fB\-\-bim\fR and/or {i:j} expansion
.TP
\fB\-\-geneticMapFile\fR arg
Oxford\-format file for interpolating genetic
distances: tables/genetic_map_hg##.txt.gz
.TP
\fB\-\-remove\fR arg
file(s) listing individuals to ignore (no
header; FID IID must be first two columns)
.TP
\fB\-\-exclude\fR arg
file(s) listing SNPs to ignore (no header;
SNP ID must be first column)
.TP
\fB\-\-maxMissingPerSnp\fR arg (=0.1)
QC filter: max missing rate per SNP
.HP
\fB\-\-maxMissingPerIndiv\fR arg (=0.1) QC filter: max missing rate per person
.TP
\fB\-\-phenoFile\fR arg
phenotype file (header required; FID IID must
be first two columns)
.TP
\fB\-\-phenoCol\fR arg
phenotype column header
.TP
\fB\-\-phenoUseFam\fR
use last (6th) column of .fam file as
phenotype
.TP
\fB\-\-covarFile\fR arg
covariate file (header required; FID IID must
be first two columns)
.TP
\fB\-\-covarCol\fR arg
categorical covariate column(s); for >1, use
multiple \fB\-\-covarCol\fR and/or {i:j} expansion
.TP
\fB\-\-qCovarCol\fR arg
quantitative covariate column(s); for >1, use
multiple \fB\-\-qCovarCol\fR and/or {i:j} expansion
.TP
\fB\-\-covarUseMissingIndic\fR
include samples with missing covariates in
analysis via missing indicator method
(default: ignore such samples)
.TP
\fB\-\-reml\fR
run variance components analysis to precisely
estimate heritability (but not compute assoc
stats)
.TP
\fB\-\-lmm\fR
compute assoc stats under the inf model and
with Bayesian non\-inf prior (VB approx), if
power gain expected
.TP
\fB\-\-lmmInfOnly\fR
compute mixed model assoc stats under the
infinitesimal model
.TP
\fB\-\-lmmForceNonInf\fR
compute non\-inf assoc stats even if BOLT\-LMM
expects no power gain
.TP
\fB\-\-modelSnps\fR arg
file(s) listing SNPs to use in model (i.e.,
GRM) (default: use all non\-excluded SNPs)
.TP
\fB\-\-LDscoresFile\fR arg
LD Scores for calibration of Bayesian assoc
stats: tables/LDSCORE.1000G_EUR.tab.gz
.TP
\fB\-\-numThreads\fR arg (=1)
number of computational threads
.TP
\fB\-\-statsFile\fR arg
output file for assoc stats at PLINK
genotypes
.TP
\fB\-\-dosageFile\fR arg
file(s) containing imputed SNP dosages to
test for association (see manual for format)
.TP
\fB\-\-dosageFidIidFile\fR arg
file listing FIDs and IIDs of samples in
dosageFile(s), one line per sample
.TP
\fB\-\-statsFileDosageSnps\fR arg
output file for assoc stats at dosage format
genotypes
.TP
\fB\-\-impute2FileList\fR arg
list of [chr file] pairs containing IMPUTE2
SNP probabilities to test for association
.TP
\fB\-\-impute2FidIidFile\fR arg
file listing FIDs and IIDs of samples in
IMPUTE2 files, one line per sample
.TP
\fB\-\-impute2MinMAF\fR arg (=0)
MAF threshold on IMPUTE2 genotypes; lower\-MAF
SNPs will be ignored
.TP
\fB\-\-bgenFile\fR arg
file(s) containing Oxford BGEN\-format
genotypes to test for association
.TP
\fB\-\-sampleFile\fR arg
file containing Oxford sample file
corresponding to BGEN file(s)
.TP
\fB\-\-bgenSampleFileList\fR arg
list of [bgen sample] file pairs containing
BGEN imputed variants to test for association
.TP
\fB\-\-bgenMinMAF\fR arg (=0)
MAF threshold on Oxford BGEN\-format
genotypes; lower\-MAF SNPs will be ignored
.TP
\fB\-\-bgenMinINFO\fR arg (=0)
INFO threshold on Oxford BGEN\-format
genotypes; lower\-INFO SNPs will be ignored
.TP
\fB\-\-statsFileBgenSnps\fR arg
output file for assoc stats at BGEN\-format
genotypes
.TP
\fB\-\-statsFileImpute2Snps\fR arg
output file for assoc stats at IMPUTE2 format
genotypes
.TP
\fB\-\-dosage2FileList\fR arg
list of [map dosage] file pairs with 2\-dosage
SNP probabilities (Ricopili/plink2 \fB\-\-dosage\fR
format=2) to test for association
.TP
\fB\-\-statsFileDosage2Snps\fR arg
output file for assoc stats at 2\-dosage
format genotypes
.PP
.SH SEE ALSO
.BR https://data.broadinstitute.org/alkesgroup/BOLT-LMM/
.SH COPYRIGHT
Copyright \(co 2014\-2018 Harvard University.
Distributed under the GNU GPLv3+ open source license.
