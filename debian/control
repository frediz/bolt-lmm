Source: bolt-lmm
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Dylan Aïssi <daissi@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper (>= 12~),
               gfortran,
               libboost-dev,
               libboost-iostreams-dev,
               libboost-program-options-dev,
               libnlopt-dev,
               libnlopt-cxx-dev,
               libopenblas-dev,
               zlib1g-dev
Standards-Version: 4.4.0
Vcs-Browser: https://salsa.debian.org/med-team/bolt-lmm
Vcs-Git: https://salsa.debian.org/med-team/bolt-lmm.git
Homepage: https://data.broadinstitute.org/alkesgroup/BOLT-LMM/

Package: bolt-lmm
Architecture: amd64
Depends: ${shlibs:Depends},
         ${misc:Depends}
Suggests: bolt-lmm-doc
Description: Efficient large cohorts genome-wide Bayesian mixed-model association testing
 The BOLT-LMM software package currently consists of two main algorithms, the
 BOLT-LMM algorithm for mixed model association testing, and the BOLT-REML
 algorithm for variance components analysis (i.e., partitioning of
 SNP-heritability and estimation of genetic correlations).
 .
 The BOLT-LMM algorithm computes statistics for testing association between
 phenotype and genotypes using a linear mixed model. By default, BOLT-LMM
 assumes a Bayesian mixture-of-normals prior for the random effect attributed
 to SNPs other than the one being tested. This model generalizes the standard
 infinitesimal mixed model used by previous mixed model association methods,
 providing an opportunity for increased power to detect associations while
 controlling false positives. Additionally, BOLT-LMM applies algorithmic
 advances to compute mixed model association statistics much faster than
 eigendecomposition-based methods, both when using the Bayesian mixture model
 and when specialized to standard mixed model association.
 .
 The BOLT-REML algorithm estimates heritability explained by genotyped SNPs and
 genetic correlations among multiple traits measured on the same set of
 individuals. BOLT-REML applies variance components analysis to perform these
 tasks, supporting both multi-component modeling to partition SNP-heritability
 and multi-trait modeling to estimate correlations. BOLT-REML applies a Monte
 Carlo algorithm that is much faster than eigendecomposition-based methods for
 variance components analysis at large sample sizes.

Package: bolt-lmm-example
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Enhances: bolt-lmm
Description: Examples for bolt-lmm
 The BOLT-LMM software package currently consists of two main algorithms, the
 BOLT-LMM algorithm for mixed model association testing, and the BOLT-REML
 algorithm for variance components analysis (i.e., partitioning of
 SNP-heritability and estimation of genetic correlations).
 .
 This package provides some example data for bolt-lmm.
